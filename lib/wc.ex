defmodule Wc do
  @principito """
  - Mi vida es monótona. Yo cazo gallinas, los hombres me cazan.
  Todas las gallinas se parecen, y todos los hombres se parecen.
  Me aburro, pues, un poco. Pero, si me domesticas, mi vida
  resultará como iluminada. Conoceré un ruido de pasos que será
  diferente de todos los demás. Los otros pasos me hacen volver
  bajo tierra. Los tuyos me llamarán fuera de la madriguera,
  como una música. Y además, mira ! Ves, allá lejos, los campos
  de trigo ? Yo no como pan. El trigo para mí es inútil. Los
  campos de trigo no me recuerdan nada. Y eso es triste ! Pero
  tú tienes cabellos color de oro. Entonces será maravilloso
  cuando me hayas domesticado ! El trigo, que es dorado, me
  hará recordarte. Y me agradará el ruido del viento en el
  trigo...
  """
  def example do
    @principito
  end
end
